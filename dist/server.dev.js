"use strict";

var mongoose = require('mongoose'); //dostęp do bazy i operacje bezdanowe


require('dotenv').config(); // obsługa pliku konfiguracyjnego .env


var express = require('express'); // wczytywanie bibliteki


var app = express(); // utworzenie obiektu frameworka - do późniejszej konfiguracji

app.use(express.json());

var carsRouter = require('./routes/cars');

app.use('/cars', carsRouter);
mongoose.connect(process.env.MONGO_DB_URL, {
  useNewUrlParser: true
});
var dbConnection = mongoose.connection;
dbConnection.on('error', function (error) {
  return console.error(error);
});
dbConnection.once('open', function () {
  return console.log("Połączonono z bazą");
});
app.listen(5555, function () {
  return console.log('uruchomiono server');
});