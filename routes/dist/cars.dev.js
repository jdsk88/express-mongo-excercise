"use strict";

var express = require('express');

var router = express.Router();

var Car = require('../models/car');

router.get('/', function _callee(req, res) {
  var cars;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return regeneratorRuntime.awrap(Car.find());

        case 3:
          cars = _context.sent;
          console.log(cars);
          res.json(cars);
          _context.next = 11;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](0);
          res.status(500).json({
            message: _context.t0.message
          });

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 8]]);
}); // router.route('/').get((req,res) => {
//     Car.find((error, cars) => {
//         if (error){
//             res.status(500).json({message: error.message})
//         } else {
//             res.json(cars)
//         }
//     })
// })

router.post('/', function _callee2(req, res) {
  var car, newCar;
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          car = new Car({
            brand: req.body.brand,
            model: req.body.model,
            buildYear: req.body.buildYear,
            color: req.body.color
          });
          _context2.prev = 1;
          _context2.next = 4;
          return regeneratorRuntime.awrap(car.save());

        case 4:
          newCar = _context2.sent;
          res.status(201).json(newCar);
          _context2.next = 11;
          break;

        case 8:
          _context2.prev = 8;
          _context2.t0 = _context2["catch"](1);
          res.status(400).json({
            message: _context2.t0.message
          });

        case 11:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[1, 8]]);
});

function getCar(req, res, next) {
  return regeneratorRuntime.async(function getCar$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return regeneratorRuntime.awrap(Car.findById(req.params.id));

        case 3:
          car = _context3.sent;

          if (!(null == car)) {
            _context3.next = 6;
            break;
          }

          return _context3.abrupt("return", res.status(404).json({
            message: "Cannot find car with given id!"
          }));

        case 6:
          _context3.next = 11;
          break;

        case 8:
          _context3.prev = 8;
          _context3.t0 = _context3["catch"](0);
          res.status(500).json({
            message: _context3.t0.message
          });

        case 11:
          res.car = car;
          next();

        case 13:
        case "end":
          return _context3.stop();
      }
    }
  }, null, null, [[0, 8]]);
}

router.get('/:id', getCar, function (req, res) {
  res.json(res.car);
});
router["delete"]('/:id', getCar, function _callee3(req, res) {
  return regeneratorRuntime.async(function _callee3$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return regeneratorRuntime.awrap(res.car.remove());

        case 3:
          res.json({
            message: " Car with given ID was removed"
          });
          _context4.next = 9;
          break;

        case 6:
          _context4.prev = 6;
          _context4.t0 = _context4["catch"](0);
          return _context4.abrupt("return", res.status(500).status({
            message: _context4.t0.message
          }));

        case 9:
        case "end":
          return _context4.stop();
      }
    }
  }, null, null, [[0, 6]]);
});
router.patch('/:id', getCar, function _callee4(req, res) {
  var updatedCar;
  return regeneratorRuntime.async(function _callee4$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          if (req.body.brand != null) {
            res.car.brand = req.body.brand;
          }

          if (req.body.model != null) {
            res.car.model = req.body.model;
          }

          if (req.body.buildYear != null) {
            res.car.buildYear = req.body.buildYear;
          }

          if (req.body.color != null) {
            res.car.color = req.body.color;
          }

          _context5.prev = 4;
          _context5.next = 7;
          return regeneratorRuntime.awrap(car.save());

        case 7:
          updatedCar = _context5.sent;
          res.json(updatedCar);
          _context5.next = 14;
          break;

        case 11:
          _context5.prev = 11;
          _context5.t0 = _context5["catch"](4);
          return _context5.abrupt("return", res.status(500).json({
            message: _context5.t0.message
          }));

        case 14:
        case "end":
          return _context5.stop();
      }
    }
  }, null, null, [[4, 11]]);
}); // router.patch('/:id', getCar, async (req,res) => {
// if (req.body.brand != null){
//     res.car.brand = req.body.brand;
// }
// if (req.body.model != null){
//     res.car.model = req.body.model;
// }
// if (req.body.buildYear != null){
//     res.car.buildYear = req.body.buildYear;
// }
// if (req.body.color != null){
//     res.car.color = req.body.color;
// }
// try {
//     const updatedCar = await car.save()
//     res.json(updatedCar)
// } catch (error) {
//     return res.status(400).json({message: error.message})
// }
// })

module.exports = router;