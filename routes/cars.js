const express = require('express');
const router = express.Router();

const Car = require('../models/car');

router.get('/', async (req, res) => {
    try {
        const cars = await Car.find()
        console.log(cars);
        res.json(cars)
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
})

// router.route('/').get((req,res) => {
//     Car.find((error, cars) => {
//         if (error){
//             res.status(500).json({message: error.message})
//         } else {
//             res.json(cars)
//         }
//     })
// })

router.post('/', async (req, res) => {
    const car = new Car({
        brand: req.body.brand,
        model: req.body.model,
        buildYear: req.body.buildYear,
        color: req.body.color,
    })

    try {
        const newCar = await car.save();
        res.status(201).json(newCar);
    } catch (error) {
        res.status(400).json({ message: error.message })
    }

})

async function getCar(req, res, next) {
    try {
        car = await Car.findById(req.params.id)
        if (null == car) {
            return res.status(404).json({ message: "Cannot find car with given id!" })
        }
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
    res.car = car
    next();
}

router.get('/:id', getCar, (req, res) => {
    res.json(res.car)
})

router.delete('/:id', getCar, async (req, res) => {
    try {
        await res.car.remove()
        res.json({ message: " Car with given ID was removed" })
    } catch (error) {
        return res.status(500).status({ message: error.message })
    }
})

router.patch('/:id', getCar, async (req, res) => {
    if (req.body.brand != null) {
        res.car.brand = req.body.brand;
    }
    if (req.body.model != null) {
        res.car.model = req.body.model;
    }
    if (req.body.buildYear != null) {
        res.car.buildYear = req.body.buildYear;
    }
    if (req.body.color != null) {
        res.car.color = req.body.color;
    }
    try {
        const updatedCar = await car.save()
        res.json(updatedCar)

    } catch (error) {
        return res.status(500).json({ message: error.message })
    }

})

// router.patch('/:id', getCar, async (req,res) => {
// if (req.body.brand != null){
//     res.car.brand = req.body.brand;
// }
// if (req.body.model != null){
//     res.car.model = req.body.model;
// }
// if (req.body.buildYear != null){
//     res.car.buildYear = req.body.buildYear;
// }
// if (req.body.color != null){
//     res.car.color = req.body.color;
// }
// try {
//     const updatedCar = await car.save()
//     res.json(updatedCar)
// } catch (error) {
//     return res.status(400).json({message: error.message})
// }

// })


module.exports = router
