const mongoose = require('mongoose') //dostęp do bazy i operacje bezdanowe
require('dotenv').config()  // obsługa pliku konfiguracyjnego .env

const express = require('express') // wczytywanie bibliteki
const app = express(); // utworzenie obiektu frameworka - do późniejszej konfiguracji

app.use( express.json());
const carsRouter = require('./routes/cars');

app.use('/cars', carsRouter);

mongoose.connect( process.env.MONGO_DB_URL, {useNewUrlParser:  true});
const dbConnection = mongoose.connection;
dbConnection.on('error', (error) => console.error(error));
dbConnection.once('open', () => console.log("Połączonono z bazą"));



app.listen(5555, ()=> console.log('uruchomiono server'))